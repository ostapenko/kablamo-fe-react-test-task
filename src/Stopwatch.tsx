import * as React from "react";
import { useCallback, useEffect, useRef, useState } from "react";
import { v4 } from "uuid";

// TODO unit tests
const formattedSeconds = (sec: number) =>
  `${Math.floor(sec / 60)}:${String(sec % 60).padStart(2, "0")}`;

interface Props {
  initialSeconds: number;
}

interface LapState {
  id: string;
  time: number;
}

const Stopwatch: React.FC<Props> = ({ initialSeconds }) => {
  const [laps, setLaps] = useState<LapState[]>([]);
  const [elapsedTimeInSeconds, setElapsedTimeInSeconds] =
    useState(initialSeconds);
  const [isRunning, setIsRunning] = useState(false);
  const timerRef = useRef<{ totalRunningTime: number; lastTickTime: number }>({
    totalRunningTime: elapsedTimeInSeconds * 1000,
    lastTickTime: 0,
  });

  useEffect(() => {
    if (!isRunning) {
      return;
    }

    timerRef.current.lastTickTime = Date.now();
    const interval = setInterval(() => {
      const now = Date.now();
      timerRef.current.totalRunningTime += now - timerRef.current.lastTickTime;
      timerRef.current.lastTickTime = now;

      setElapsedTimeInSeconds(
        Math.floor(timerRef.current.totalRunningTime / 1000),
      );
    }, 1000 / 30); // 30 FPS

    return () => {
      clearInterval(interval);
    };
  }, [isRunning]);

  const onStartTimer = useCallback(() => setIsRunning(true), []);
  const onPauseTimer = useCallback(() => setIsRunning(false), []);
  const onResetTimer = useCallback(() => {
    setIsRunning(false);
    setLaps([]);
    setElapsedTimeInSeconds(0);
    timerRef.current.totalRunningTime = 0;
  }, []);
  const onLapTimer = useCallback(() => {
    setLaps((prevState) => {
      // this can use the value of `elapsedTimeInSeconds`, though in this case
      // we would have to add this variable to the dependencies array, therefore
      // a new function will be created each second, which might have negative
      // effect on performance
      const newLap: LapState = {
        id: v4(),
        time: Math.floor(timerRef.current.totalRunningTime / 1000),
      };
      return [...prevState, newLap];
    });
  }, []);
  const onRemoveLap = useCallback((idToRemove: string) => {
    setLaps((prevState) => prevState.filter((lap) => lap.id !== idToRemove));
  }, []);

  const StartButton = (
    <button
      type="button"
      className="start-btn btn btn-primary btn-outline my-2"
      onClick={onStartTimer}
    >
      {elapsedTimeInSeconds === 0 ? "Start" : "Continue"}
    </button>
  );
  const PauseButton = (
    <button type="button" className="pause-btn btn my-2" onClick={onPauseTimer}>
      Pause
    </button>
  );
  const ResetButton = (
    <button type="button" className="reset-btn btn my-2" onClick={onResetTimer}>
      Reset
    </button>
  );
  const LapButton = (
    <button type="button" className="lap-btn btn my-2" onClick={onLapTimer}>
      Lap
    </button>
  );

  return (
    <div className="stopwatch">
      <h1 className="stopwatch-timer">
        Time: {formattedSeconds(elapsedTimeInSeconds)}
      </h1>
      {isRunning ? (
        <>
          {PauseButton} <br />
          {LapButton}
        </>
      ) : (
        <>
          {StartButton} <br />
          {ResetButton}
        </>
      )}
      <div className="divider"></div>
      <div className="stopwatch-laps">
        {laps.map((lap, i) => (
          <LapRow key={lap.id} index={i + 1} lap={lap} onDelete={onRemoveLap} />
        ))}
      </div>
    </div>
  );
};

interface LapRowProps {
  index: number;
  lap: LapState;
  onDelete: (byId: string) => void;
}

/**
 * It is possible to use `React.memo` here since this component is a simple
 * representational component without any complex business logic in it
 */
const LapRow = React.memo<LapRowProps>(({ index, lap, onDelete }) => {
  /**
   * There's no need to use `useCallback` here as the component itself is memoized
   */
  const onRemoveClicked = () => onDelete(lap.id);
  return (
    <li className="stopwatch-lap flex my-1">
      <strong>Lap №{index}</strong>: {formattedSeconds(lap.time)}
      <span> </span>
      <button className="btn btn-square btn-xs" onClick={onRemoveClicked}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-6 w-6"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="2"
            d="M6 18L18 6M6 6l12 12"
          />
        </svg>
      </button>
    </li>
  );
});

export default Stopwatch;
