import React from "react";
import Stopwatch from "./Stopwatch";
import { createRoot } from "react-dom/client";

const root = createRoot(document.getElementById("root")!);
root.render(
  <React.StrictMode>
    <main className="container mx-auto py-4 typography prose lg:prose-xl">
      <div className="mockup-window border border-base-300">
        <div className="px-4 py-4 border-t border-base-300">
          <Stopwatch initialSeconds={0} />
        </div>
      </div>
    </main>
  </React.StrictMode>,
);
