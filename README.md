# Kablamo FE React test task

## The implementation

The Node.js Version used in this project is denoted in `package.json`. If you use Volta or NVM, there are corresponding configs in this project that support these tools.

Use `npm install` and `npm start` to run the application on `localhost`.

The project is deployed here: https://kablamo-fe-react-test-task.vercel.app/.

# Issues in the original solution

Here is a non-exhaustive list of issues and imperfections found in the source code.
With each highlighted item I included several possible ways to address the problem, with exampled of code where it was concise.

## Incorrect passing of callbacks

There are several places where callbacks are passed incorrectly to the components of the application:

```tsx
onClick={this.handleStartClick}
```

Since all callbacks (`handleStartClick`, `handleStopClick`) are declared as class functions, they don't have a context associated with them. Meaning that being called, `this` inside the functions will be undefined and the code will throw errors. To correct this issue, there are several possible ways:

- bind the context to these functions in the component constructor:

```tsx
constructor () {
  ///
  
  this.handleStartClick = this.handleStartClick.bind(this);
}
```

- instead of bare functions, pass curried callbacks:

```tsx
onClick={this.handleStartClick.bind(this)}
```

This approach will have negative impact on the performance, since a new function is created on each `render` call, which might trigger unnecessary re-renders.

- declare callbacks as class properties holding arrow functions:

```tsx
class Stopwatch {
  ///
  
  handleStartClick: () => {
    this.incrementer = setInterval( /* ... */ );
  }
}
```

This way the context will be 'locked' to the arrow function and there won't be any issues with `this` inside the functions.


### Function Components

A good approach to define callbacks in Function React Components is to use certain hooks, like `useCallback`, that allows us to define callback functions that don't change between render calls:

```tsx
import { useCallback } from "react";

const handleStartClick = useCallback(() => {
  setInterval( /* ... */ );
}, []) // <- dependency array allows us to specify conditions when the callback should change
```

## Incorrect work with the component's state

The parts of the state, such as `laps` and `incrementer` are not stored in the component's `state` but as class fields instead. React doesn't track the changes to such fields. As a result, we might get a component's DOM that does not correctly represent the component's state. If we want React to trigger re-render of the component's representation whenever some part of the application state is changed, we should use `this.state` to store such parts of the state and `this.setState` to update those.

In the original source code there could be found a `forceUpdate` call in one place, when we mutate the `laps` array. Despite, this being a possible way to trigger re-render of the component's DOM, it still is a bad practice for several reasons:
- Using `forceUpdate` can unnecessarily bypass React rendering mechanism, and lead to inefficient updates, as React is designed to re-render components only when their data has changed
- It is easy to miss adding a `forceUpdate` everywhere when we mutate parts of the state that are not managed my React's state itself
- By not allowing React to manage component updates efficiently, you risk impacting the performance of your application. React optimizes rendering to be as efficient as possible, but using `forceUpdate` can undermine these optimizations.

When using `this.setState`, it is important to update references to all state parts and NOT to change state fields in-place, as React checks if the state objects are the same, not necessarily their internal values.

```tsx
    handleLapClick() {
        // this.laps = this.laps.concat([this.state.secondsElapsed]);
        this.setState(prevState => {
          const { laps, secondsElapsed } = prevState;
          return { laps: [...laps, secondsElapsed] };
        });
    }
```

### Function Components

Use `useState` hook to declare parts of the state and get a callback function that updates that state:

```tsx
const [ laps, setLaps ] = useState([]);
const [ secondsElapsed, setSecondsElapsed ] = useState(initialSeconds);

const handleStartClick = useCallback(() => {
  setLaps(originalLaps => ([...originalLaps, secondsElapsed]));
}, [secondsElapsed]);
```

In more advanced use-cases it might make more sense to use `useRef` and keep track of the state changes 'manually'.

## Incorrect rendering of the arrays

In this part of the application in order to avoid rendering issues, we must add a prop `key` to the component, that we return from the `map` cycle. Keys only make sense in the context of the surrounding array. Therefore, the following usage is incorrect, as we should return `key` from the body of a `map` cycle. 

Instead of:
```tsx
<div className="stopwatch-laps">
  {
    this.laps && this.laps.map((lap, i) => (
      <Lap index={i + 1} lap={lap} onDelete={this.handleDeleteClick(i)} />
    ))
  }
</div>
```

It should be:
```tsx
<div className="stopwatch-laps">
  {
    this.laps && this.laps.map((lap, i) => (
      <Lap key={i} index={i + 1} lap={lap} onDelete={this.handleDeleteClick(i)} />
    ))
  }
</div>
```

### Index as a key is an anti-pattern

Generally speaking, using an array's Index as a key is an anti-pattern. If we change the contents of the array, for example by sorting it, the old indexes wouldn't correspond to the same data anymore and our application may break. More stable approach is to have or **assign** some unique id to each element in the array and to use it as a `key`:

```tsx
handleLapClick() {
    this.setState(prevState => {
      const { laps, secondsElapsed } = prevState;
      return { laps: [...laps, { value: secondsElapsed, key: uuid() }] };
    });
}

render () {
    return (
      <div className="stopwatch-laps">
        {
          this.laps && this.laps.map((lap, i) => (
            <Lap key={lap.key} index={i + 1} lap={lap.value} onDelete={this.handleDeleteClick(i)} />
          ))
        }
      </div>
    )
}
```

### Callbacks inside Lists

It is worth noting that here `onDelete={this.handleDeleteClick(i)}` we are actually passing a prop callback correctly, i.e. without any errors. This is because function `handleDeleteClick` actually **returns** a new callback function with its closure, and this allows us to have a correct `this` and `index` whenever `onDelete` is called.

Although, this approach works, this might not be the best approach in terms of performance, as there still new functions are created on each render call. Depending on the size of the List, this might cause some performance issues. To remediate this, it might be worth memorizing the `this.handleDeleteClick(i)` function, or refactoring the code in a way that `onDelete` accepts a constant callback.

## Wrong reliance on the JavaScript timers

```tsx
this.incrementer = setInterval(
    () =>
        this.setState({
            secondsElapsed: this.state.secondsElapsed + 1,
        }),
    1000,
);
```

Here we assume that the callback is called once every second, which is, strongly speaking, not a case. JavaScript engine doesn't guarantee the precise work of such timers and in many cases these timers can be called later than expected:
- if the browser's event-loop is busy
- if the engine is performing a long synchronous computation
- if the browser's tab is out of focus and the OS throttles the CPU given to the process

To make the application more reliable, it is always a good idea to save timestamps when the timers' callbacks are executed, and calculate the time passed as a difference between the last iteration and the current iteration:

```tsx
let last = Date.now();
let incrementer = setInterval(
    () => {
      const timePassed = Date.now() - last;
      last = Date.now();
      ...
    },
    1000,
);
```

If we need more precise and granular time control, it is a good idea to reduce the Interval with which the callback is requested, or, even, to utilise `requestAnimationFrame` to ensure our application is as responsive as possible.

## The lack of tests

It is a good practice to add tests into the application to ensure that the user experience doesn't deteriorate. It is a good thing that some parts or the application are already easily testable. Namely, the `formattedSeconds` function is a pure function, which makes it simple to test.

Generally speaking, it is a good approach to avoid coupling and writing God-like functions and classes, make modules simple and pure, and to write tests to maintain the functionality of the application.

## Incomplete utilisation of TypeScript

Some parts of the application don't take full advantage of using TypeScript in the project. When using `any` instead of declaring actual interfaces and types of the data in the application, we deprive ourselves of extra static analysis checks and of enhanced IDE assistance.

## Not using CSS Modules

The description of the task doesn't mention what approach this application uses for styling the UI. Depending on the size of the system, it might be a good idea to use CSS Modules to avoid any issues with the mixing and duplicating CSS classes across entire application codebase.
